package com.apiep.form.task;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import com.apiep.model.Task;
import com.apiep.utils.Database;

public class TaskController implements Initializable {
	@FXML
	private TextField textId;
	@FXML
	private TextField textJudul;
	@FXML
	private TextArea textKeterangan;
	@FXML
	private CheckBox checkSelesai;
	@FXML
	private TableView<Task> tableView;

	@FXML
	private TableColumn<Task, Long> kolomId;
	@FXML
	private TableColumn<Task, Boolean> kolomSelesai;
	@FXML
	private TableColumn<Task, String> kolomJudul;
	@FXML
	private TableColumn<Task, String> kolomKeterangan;

	private Database db = new Database();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		kolomId.setCellValueFactory(new PropertyValueFactory<Task, Long>("id"));
		kolomJudul.setCellValueFactory(new PropertyValueFactory<Task, String>("judul"));
		kolomKeterangan.setCellValueFactory(new PropertyValueFactory<Task, String>("keterangan"));
		kolomSelesai.setCellValueFactory(new PropertyValueFactory<Task, Boolean>("selesai"));
		kolomSelesai.setCellFactory(new Callback<TableColumn<Task, Boolean>, TableCell<Task, Boolean>>() {
			@Override
			public TableCell<Task, Boolean> call(TableColumn<Task, Boolean> arg0) {
				return new CheckBoxTableCell<Task, Boolean>();
			}
		});
		tableView.getItems().setAll(db.getTask());
		tableView.setOnMouseClicked(new EventHandler<MouseEvent>(){
			@Override
			public void handle(MouseEvent arg0) {
				Task task = tableView.getSelectionModel().getSelectedItem();
				textId.setText(String.valueOf(task.getId()));
				textJudul.setText(task.getJudul());
				textKeterangan.setText(task.getKeterangan());
				checkSelesai.setSelected(task.isSelesai());
			}
		});
	}

	@FXML
	public void btnTambahClicked(ActionEvent event) {
		Task task = new Task();
		task.setJudul(textJudul.getText());
		task.setKeterangan(textKeterangan.getText());
		task.setSelesai(checkSelesai.isSelected());
		db.addTask(task);
		textId.setText(String.valueOf(task.getId()));
		tableView.getItems().add(task);
	}

	@FXML
	public void btnSimpanClicked(ActionEvent event) {
		Task task = tableView.getFocusModel().getFocusedItem();
		task.setId(Long.valueOf(textId.getText()));
		task.setJudul(textJudul.getText());
		task.setKeterangan(textKeterangan.getText());
		task.setSelesai(checkSelesai.isSelected());
		db.updateTask(task);
	}

	public static class CheckBoxTableCell<S, T> extends TableCell<S, T> {
		private final CheckBox checkbox;
		private ObservableValue<T> ov;

		public CheckBoxTableCell() {
			this.checkbox = new CheckBox();
			this.checkbox.setAlignment(Pos.CENTER);
			setAlignment(Pos.CENTER);
			setGraphic(checkbox);
		}

		@Override
		public void updateItem(T item, boolean empty) {
			if (empty) {
				setText(null);
				setGraphic(null);
			} else {
				setGraphic(checkbox);
				if (ov instanceof BooleanProperty) {
					checkbox.selectedProperty().unbindBidirectional((BooleanProperty) ov);
				}
				ov = getTableColumn().getCellObservableValue(getIndex());
				if (ov instanceof BooleanProperty) {
					checkbox.selectedProperty().bindBidirectional((BooleanProperty) ov);
				}
			}
		}
	}
}
