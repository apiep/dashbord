package com.apiep.form.task;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import com.apiep.model.Task;

public class TableTaskView extends TableView<Task> {

	private TableColumn<Task, Long> kolomId;
	private TableColumn<Task, String> kolomJudul;
	private TableColumn<Task, String> kolomKeterangan;
	private TableColumn<Task, Boolean> kolomSelesai;

	@SuppressWarnings("unchecked")
	public TableTaskView() {
		kolomId = new TableColumn<Task, Long>();
		kolomJudul = new TableColumn<Task, String>();
		kolomKeterangan = new TableColumn<Task, String>();
		kolomSelesai = new TableColumn<Task, Boolean>();

		kolomId.setText("ID");
		kolomJudul.setText("Judul");
		kolomKeterangan.setText("Keterangan");
		kolomSelesai.setText("Selesai?");

		kolomId.setCellValueFactory(new PropertyValueFactory<Task, Long>("id"));
		kolomJudul.setCellValueFactory(new PropertyValueFactory<Task, String>("judul"));
		kolomKeterangan.setCellValueFactory(new PropertyValueFactory<Task, String>("keterangan"));
		kolomSelesai.setCellValueFactory(new PropertyValueFactory<Task, Boolean>("selesai"));
		
		getColumns().addAll(kolomId, kolomJudul, kolomKeterangan, kolomSelesai);
	}
}
