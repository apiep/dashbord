package com.apiep.form.pengaturan;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooserBuilder;

import com.apiep.utils.Konfigurasi;

public class PengaturanController implements Initializable {

	@FXML
	private TextField textDbPath;
	@FXML
	private Button btnSimpan;
	@FXML
	private Button btnBatal;

	private Konfigurasi konf = new Konfigurasi();
	private String dbPathSekarang = "";
	private String dbPathBaru = "";

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		textDbPath.setText(konf.getDbPath());
		dbPathSekarang = konf.getDbPath();
		btnSimpan.setDisable(true);
		btnBatal.setDisable(true);
	}

	@FXML
	public void btnCariClicked(ActionEvent event) {
		FileChooser fileChooser = FileChooserBuilder.create()
				.initialDirectory(new File(System.getProperty("user.home")))
				.title("Path database").build();
		File file = fileChooser.showOpenDialog(textDbPath.getScene()
				.getWindow());
		textDbPath.setText(file.getAbsolutePath());
		dbPathBaru = file.getAbsolutePath();
		btnSimpan.setDisable(false);
		btnBatal.setDisable(false);
	}

	@FXML
	public void btnSimpanClicked(ActionEvent event) {
		konf.setDbPath(dbPathBaru);
		btnSimpan.setDisable(true);
		btnBatal.setDisable(true);
	}

	@FXML
	public void btnBatalClicked(ActionEvent event) {
		textDbPath.setText(dbPathSekarang);
		btnSimpan.setDisable(true);
		btnBatal.setDisable(true);
	}
}
