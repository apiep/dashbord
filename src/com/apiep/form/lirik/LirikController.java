package com.apiep.form.lirik;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import com.apiep.model.Lirik;
import com.apiep.utils.Database;

public class LirikController implements Initializable {

	@FXML
	private TextField textId;
	@FXML
	private TextField textJudul;
	@FXML
	private TextField textArtis;
	@FXML
	private TextArea textLirik;
	@FXML
	private TextArea textLirikView;
	@FXML
	private ListView<Lirik> listLirik;

	private Database db = new Database();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		listLirik.getItems().setAll(db.getLirik());
	}

	@FXML
	public void btnSimpanClicked(ActionEvent event) {
		Lirik lirik = listLirik.getFocusModel().getFocusedItem();
		lirik.setId(Long.valueOf(textId.getText()));
		lirik.setJudul(textJudul.getText());
		lirik.setArtis(textArtis.getText());
		lirik.setLirik(textLirik.getText());
		db.updateLirik(lirik);
		textLirikView.setText(lirik.getLirik());
	}

	@FXML
	public void btnTambahClicked(ActionEvent event) {
		Lirik lirik = new Lirik();
		lirik.setArtis(textArtis.getText());
		lirik.setJudul(textJudul.getText());
		lirik.setLirik(textLirik.getText());
		db.addLirik(lirik);
		listLirik.getItems().add(lirik);
		textId.setText(String.valueOf(lirik.getId()));
		textLirikView.setText(lirik.getLirik());
	}

	@FXML
	public void listLirikClicked(MouseEvent event) {
		Lirik lirik = listLirik.getSelectionModel().getSelectedItem();
		if (lirik != null) {
			textLirikView.setText(lirik.getLirik());
			textId.setText(String.valueOf(lirik.getId()));
			textJudul.setText(lirik.getJudul());
			textArtis.setText(lirik.getArtis());
			textLirik.setText(lirik.getLirik());
		}
	}
}
