package com.apiep.form.note;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import com.apiep.model.Note;
import com.apiep.utils.Database;

public class NoteController implements Initializable {

	@FXML
	private TextField textId;
	@FXML
	private TextField textJudul;
	@FXML
	private TextArea textKeterangan;
	@FXML
	private TextArea textNoteView;
	@FXML
	private ListView<Note> listNote;

	private Database db = new Database();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		listNote.getItems().setAll(db.getNote());
	}

	@FXML
	public void btnTambahClicked(ActionEvent event) {
		Note note = new Note();
		note.setJudul(textJudul.getText());
		note.setKonten(textKeterangan.getText());
		db.addNote(note);
		textId.setText(String.valueOf(note.getId()));
		listNote.getItems().add(note);
		textNoteView.setText(note.getKonten());
	}

	@FXML
	public void btnSimpanClicked(ActionEvent event) {
		Note note = listNote.getFocusModel().getFocusedItem();
		note.setId(Long.valueOf(textId.getText()));
		note.setJudul(textJudul.getText());
		note.setKonten(textKeterangan.getText());
		db.updateNote(note);
		textNoteView.setText(note.getKonten());
	}

	@FXML
	public void listNoteClicked(MouseEvent event) {
		Note note = listNote.getSelectionModel().getSelectedItem();
		if (note != null) {
			textNoteView.setText(note.getKonten());
			textId.setText(String.valueOf(note.getId()));
			textJudul.setText(note.getJudul());
			textKeterangan.setText(note.getKonten());
		}
	}
}
