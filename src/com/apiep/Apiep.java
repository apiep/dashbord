package com.apiep;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Apiep extends Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		BorderPane root = new BorderPane();
		Scene scene = new Scene(root, 650, 500);
		scene.setFill(Color.BLACK);

		TabPane mainPane = new TabPane();
		root.setCenter(mainPane);

		Tab tabLirik = new Tab("Lirik");
		tabLirik.setClosable(false);
		Parent panelLirik = FXMLLoader.load(getClass().getResource(
				"/com/apiep/form/lirik/Lirik.fxml"));
		AnchorPane paneLirik = new AnchorPane();
		paneLirik.getChildren().add(panelLirik);
		AnchorPane.setTopAnchor(panelLirik, 0.0);
		AnchorPane.setRightAnchor(panelLirik, 0.0);
		AnchorPane.setBottomAnchor(panelLirik, 0.0);
		AnchorPane.setLeftAnchor(panelLirik, 0.0);
		tabLirik.setContent(paneLirik);
		mainPane.getTabs().add(tabLirik);

		Tab tabNote = new Tab("Note");
		tabNote.setClosable(false);
		Parent panelNote = FXMLLoader.load(getClass().getResource(
				"/com/apiep/form/note/Note.fxml"));
		AnchorPane paneNote = new AnchorPane();
		paneNote.getChildren().add(panelNote);
		AnchorPane.setTopAnchor(panelNote, 0.0);
		AnchorPane.setRightAnchor(panelNote, 0.0);
		AnchorPane.setBottomAnchor(panelNote, 0.0);
		AnchorPane.setLeftAnchor(panelNote, 0.0);
		tabNote.setContent(paneNote);
		mainPane.getTabs().add(tabNote);

		Tab tabTask = new Tab("Task");
		tabTask.setClosable(false);
		Parent panelTask = FXMLLoader.load(getClass().getResource(
				"/com/apiep/form/task/Task.fxml"));
		AnchorPane paneTask = new AnchorPane();
		paneTask.getChildren().add(panelTask);
		AnchorPane.setTopAnchor(panelTask, 0.0);
		AnchorPane.setRightAnchor(panelTask, 0.0);
		AnchorPane.setBottomAnchor(panelTask, 0.0);
		AnchorPane.setLeftAnchor(panelTask, 0.0);
		tabTask.setContent(paneTask);
		mainPane.getTabs().add(tabTask);
		
		Tab tabPengaturan = new Tab("Pengaturan");
		tabPengaturan.setClosable(false);
		Parent panelPengaturan = FXMLLoader.load(getClass().getResource("/com/apiep/form/pengaturan/Pengaturan.fxml"));
		AnchorPane panePengaturan = new AnchorPane();
		panePengaturan.getChildren().add(panelPengaturan);
		AnchorPane.setTopAnchor(panelPengaturan, 0.0);
		AnchorPane.setRightAnchor(panelPengaturan, 0.0);
		AnchorPane.setBottomAnchor(panelPengaturan, 0.0);
		AnchorPane.setLeftAnchor(panelPengaturan, 0.0);
		tabPengaturan.setContent(panePengaturan);
		mainPane.getTabs().add(tabPengaturan);

		primaryStage.setTitle("Apiep");
		primaryStage.setMinHeight(500);
		primaryStage.setMinWidth(650);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
