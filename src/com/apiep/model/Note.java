package com.apiep.model;

public class Note {
	private Long id = 0L;

	private String judul;
	private String konten;

	public Note() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJudul() {
		return judul;
	}

	public void setJudul(String judul) {
		this.judul = judul;
	}

	public String getKonten() {
		return konten;
	}

	public void setKonten(String konten) {
		this.konten = konten;
	}
	
	public String toString() {
		return this.judul;
	}
}
