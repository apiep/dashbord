package com.apiep.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class Task {
	
	private LongProperty id;
	private StringProperty judul;
	private StringProperty keterangan;
	private BooleanProperty selesai;

	public Task() {
		id = new SimpleLongProperty();
		judul = new SimpleStringProperty();
		keterangan = new SimpleStringProperty();
		selesai = new SimpleBooleanProperty();
		
		selesai.addListener(new ChangeListener<Boolean>(){
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
				System.out.println("Value terganti");
			}
		});
	}

	public Long getId() {
		return id.get();
	}

	public void setId(Long id) {
		this.id.set(id);
	}

	public String getJudul() {
		return judul.get();
	}

	public void setJudul(String judul) {
		this.judul.set(judul);
	}

	public String getKeterangan() {
		return keterangan.get();
	}

	public void setKeterangan(String keterangan) {
		this.keterangan.set(keterangan);
	}

	public boolean isSelesai() {
		return selesai.get();
	}

	public void setSelesai(boolean selesai) {
		this.selesai.set(selesai);
	}
	
	public LongProperty idProperty(){
		return id;
	}
	public BooleanProperty selesaiProperty() {
		return selesai;
	}
	public StringProperty judulProperty() {
		return judul;
	}
	public StringProperty keteranganProperty() {
		return keterangan;
	}
}
