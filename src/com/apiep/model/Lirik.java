package com.apiep.model;

public class Lirik {
	private Long id = 0L;
	private String artis;
	private String judul;
	private String lirik;

	public Lirik() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getArtis() {
		return artis;
	}

	public void setArtis(String artis) {
		this.artis = artis;
	}

	public String getJudul() {
		return judul;
	}

	public void setJudul(String judul) {
		this.judul = judul;
	}

	public String getLirik() {
		return lirik;
	}

	public void setLirik(String lirik) {
		this.lirik = lirik;
	}
	
	public String toString() {
		return this.artis + " - " + this.judul;
	}
}
