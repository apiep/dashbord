package com.apiep.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteJDBCLoader;

import com.apiep.model.Lirik;
import com.apiep.model.Note;
import com.apiep.model.Task;

public class Database {
	private Connection conn;
	private Statement st;
	private PreparedStatement pst;

	private Konfigurasi konf;
	private String url = "jdbc:sqlite:/";

	public Database() {
		konf = new Konfigurasi();
		SQLiteJDBCLoader.initialize();
		try {
			conn = new SQLiteConfig().createConnection(url + konf.getDbPath());
			initDb();
		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void initDb() {
		String sqlLirik = "CREATE TABLE IF NOT EXISTS lirik ("
				+ " id integer primary key autoincrement," + " judul text,"
				+ " artis text," + " lirik text" + ");";
		String sqlNote = "CREATE TABLE IF NOT EXISTS note ("
				+ " id integer primary key autoincrement," + " judul text,"
				+ " konten text" + ");";
		String sqlTask = "CREATE TABLE IF NOT EXISTS task ("
				+ " id integer primary key autoincrement," + " judul text,"
				+ " keterangan text," + " selesai bool" + ");";
		try {
			st = conn.createStatement();
			st.addBatch(sqlLirik);
			st.addBatch(sqlNote);
			st.addBatch(sqlTask);
			st.executeBatch();
		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}

	}

	public void addNote(Note note) {
		try {
			String sql = "INSERT INTO note (judul,konten) VALUES (?, ?)";
			pst = conn.prepareStatement(sql, new String[] { "id" });
			pst.setString(1, note.getJudul());
			pst.setString(2, note.getKonten());
			pst.executeUpdate();
			ResultSet rs = pst.getGeneratedKeys();
			while (rs.next()) {
				note.setId(rs.getLong(1));
			}
		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
	}

	public List<Note> getNote() {
		List<Note> list = new ArrayList<>();
		try {
			String sql = "SELECT * FROM note";
			st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Note note = new Note();
				note.setId(rs.getLong("id"));
				note.setJudul(rs.getString("judul"));
				note.setKonten(rs.getString("konten"));
				list.add(note);
			}
		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
		return list;
	}

	public void updateNote(Note note) {
		try {
			String sql = "UPDATE note SET judul=?,konten=? WHERE id=?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, note.getJudul());
			pst.setString(2, note.getKonten());
			pst.setLong(3, note.getId());
			pst.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void deleteNote(Note note) {
		try {
			String sql = "DELETE FROM note WHERE id=?";
			pst = conn.prepareStatement(sql);
			pst.setLong(1, note.getId());
			pst.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void addLirik(Lirik lirik) {
		try {
			String sql = "INSERT INTO lirik (judul,artis,lirik) VALUES (?,?,?)";
			pst = conn.prepareStatement(sql, new String[] { "id" });
			pst.setString(1, lirik.getJudul());
			pst.setString(2, lirik.getArtis());
			pst.setString(3, lirik.getLirik());
			pst.executeUpdate();
			ResultSet rs = pst.getGeneratedKeys();
			while (rs.next()) {
				lirik.setId(rs.getLong(1));
			}
		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
	}

	public List<Lirik> getLirik() {
		List<Lirik> list = new ArrayList<>();
		try {
			String sql = "SELECT * FROM lirik";
			st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Lirik lirik = new Lirik();
				lirik.setId(rs.getLong("id"));
				lirik.setJudul(rs.getString("judul"));
				lirik.setArtis(rs.getString("artis"));
				lirik.setLirik(rs.getString("lirik"));
				list.add(lirik);
			}
		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
		return list;
	}

	public void updateLirik(Lirik lirik) {
		try {
			String sql = "UPDATE lirik SET judul=?,artis=?,lirik=? WHERE id=?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, lirik.getJudul());
			pst.setString(2, lirik.getArtis());
			pst.setString(3, lirik.getLirik());
			pst.setLong(4, lirik.getId());
			pst.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void deleteLirik(Lirik lirik) {
		try {
			String sql = "DELETE FROM lirik WHERE id=?";
			pst = conn.prepareStatement(sql);
			pst.setLong(1, lirik.getId());
			pst.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void addTask(Task task) {
		try {
			String sql = "INSERT INTO task (judul,keterangan,selesai)"
					+ "VALUES (?,?,?)";
			pst = conn.prepareStatement(sql, new String[] { "id" });
			pst.setString(1, task.getJudul());
			pst.setString(2, task.getKeterangan());
			pst.setBoolean(3, task.isSelesai());
			pst.executeUpdate();
			ResultSet rs = pst.getGeneratedKeys();
			while (rs.next()) {
				task.setId(rs.getLong(1));
			}
		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
	}

	public List<Task> getTask() {
		List<Task> list = new ArrayList<>();
		try {
			String sql = "SELECT * FROM task";
			st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Task task = new Task();
				task.setId(rs.getLong("id"));
				task.setJudul(rs.getString("judul"));
				task.setKeterangan(rs.getString("keterangan"));
				task.setSelesai(rs.getBoolean("selesai"));
				list.add(task);
			}
		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
		return list;
	}

	public void updateTask(Task task) {
		try {
			String sql = "UPDATE task SET judul=?,keterangan=?,selesai=?"
					+ " WHERE id=?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, task.getJudul());
			pst.setString(2, task.getKeterangan());
			pst.setBoolean(3, task.isSelesai());
			pst.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void deleteTask(Task task) {
		try {
			String sql = "DELETE FROM task WHERE id=?";
			pst = conn.prepareStatement(sql);
			pst.setLong(1, task.getId());
			pst.executeUpdate();
		} catch (SQLException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
	}
}
