package com.apiep.utils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Konfigurasi {
	private File file;

	public Konfigurasi() {
		file = new File("config.json");
	}

	private FileWriter getFileToWrite(File file) throws IOException {
		return new FileWriter(file);
	}

	private FileReader getFileToRead(File file) throws IOException {
		return new FileReader(file);
	}

	public String getDbPath() {
		JSONObject jsonObject = new JSONObject();
		JSONParser jsonParser = new JSONParser();
		try {
			jsonObject = (JSONObject) jsonParser.parse(getFileToRead(file));
		} catch (IOException | ParseException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
		return (String) jsonObject.get("dbPath");
	}

	@SuppressWarnings("unchecked")
	public void setDbPath(String path) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("dbPath", path);
		try (FileWriter writer = getFileToWrite(file)) {
			writer.write(jsonObject.toJSONString());
		} catch (IOException ex) {
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
		}
	}
}
