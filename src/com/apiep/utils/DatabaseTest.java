package com.apiep.utils;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import com.apiep.model.Lirik;
import com.apiep.model.Note;
import com.apiep.model.Task;

public class DatabaseTest {

	Database db;

	@Before
	public void initialize() {
		db = new Database();
	}

	@Test
	public void testAddNote() {
		Note note = new Note();
		note.setJudul("Judul 1");
		note.setKonten("Konten note pertama");
		db.addNote(note);

		Note note2 = new Note();
		note2.setJudul("Judul 2");
		note2.setKonten("Konten note kedua");
		db.addNote(note2);

		Note note3 = new Note();
		note3.setJudul("Judul 3");
		note3.setKonten("Konten note ketiga");
		db.addNote(note3);
	}

	@Test
	public void testGetAllNote() {
		for (Note note : db.getNote()) {
			System.out.println("ID note => " + note.getId());
			System.out.println("Judul note => " + note.getJudul());
			System.out.println("Konten note => " + note.getKonten());
			System.out.println("==================================");
		}
	}

	@Test
	public void testUpdateNote() {
		Note note = new Note();
		note.setId(1L);
		note.setJudul("Judulku tak ganti");
		note.setKonten("Coba mengganti judul note yg memiliki ID 1");
		db.updateNote(note);
	}

	@Test
	public void testDeleteNote() {
		Note note = new Note();
		note.setId(2L);
		db.deleteNote(note);
	}

	@Test
	public void testAddLirik() {
		Lirik lirik = new Lirik();
		lirik.setArtis("Artis 1");
		lirik.setJudul("Judu dari artis 1");
		lirik.setLirik("Ini contoh lirik dari artis ke 1");
		db.addLirik(lirik);

		Lirik lirik2 = new Lirik();
		lirik2.setArtis("Artis 2");
		lirik2.setJudul("Judu dari artis 2");
		lirik2.setLirik("Ini contoh lirik dari artis ke 2");
		db.addLirik(lirik2);

		Lirik lirik3 = new Lirik();
		lirik3.setArtis("Artis 3");
		lirik3.setJudul("Judu dari artis 3");
		lirik3.setLirik("Ini contoh lirik dari artis ke 3");
		db.addLirik(lirik3);
	}

	@Test
	public void testGetLirik() {
		for (Lirik lirik : db.getLirik()) {
			System.out.println("ID lirik => " + lirik.getId());
			System.out.println("Judul lirik => " + lirik.getJudul());
			System.out.println("Artis lirik => " + lirik.getArtis());
			System.out.println("Lirik lirik => " + lirik.getLirik());
			System.out.println("==================================");
		}
	}

	@Test
	public void testUpdateLirik() {
		Lirik lirik = new Lirik();
		lirik.setId(1L);
		lirik.setJudul("Ini lirik yang sudah diupdate");
		lirik.setArtis("Artis lirik ke 1 yg diupdate");
		lirik.setLirik("Isi lirik yg sudah diupdate");
		db.updateLirik(lirik);
	}

	@Test
	public void testDeleteLirik() {
		Lirik lirik = new Lirik();
		lirik.setId(2L);
	}

	@Test
	public void testAddTask() {
		Task task = new Task();
		task.setJudul("Judul task 1");
		task.setKeterangan("Isi keterangan task ke 1");
		task.setSelesai(false);
		Calendar tanggal_dibuat = Calendar.getInstance();
		tanggal_dibuat.set(2012, Calendar.FEBRUARY, 27);
		db.addTask(task);

		Task task2 = new Task();
		task2.setJudul("Judul task 2");
		task2.setKeterangan("Isi keterangan task ke 2");
		task2.setSelesai(true);
		Calendar tanggal_dibuat2 = Calendar.getInstance();
		tanggal_dibuat2.set(2012, Calendar.DECEMBER, 27);
		Calendar tanggal_selesai2 = Calendar.getInstance();
		tanggal_selesai2.set(2013, Calendar.JANUARY, 28);
		db.addTask(task2);

		Task task3 = new Task();
		task3.setJudul("Judul task 3");
		task3.setKeterangan("Isi keterangan task ke 3");
		task3.setSelesai(false);
		Calendar tanggal_dibuat3 = Calendar.getInstance();
		tanggal_dibuat3.set(2011, Calendar.FEBRUARY, 27);
		db.addTask(task3);
	}

	@Test
	public void testGetTask() {
		System.out.println("Get task ==>");
		for (Task task : db.getTask()) {
			System.out.println("ID task => " + task.getId());
			System.out.println("Judul task => " + task.getJudul());
			System.out.println("Keterangan task => " + task.getKeterangan());
			System.out.println("Selesai task => " + task.isSelesai());
			System.out.println("==================================");
		}
	}

	@Test
	public void testUpdateTask() {
		Task task = new Task();
		task.setJudul("Judul task 3 yang sudah di update");
		task.setKeterangan("Isi keterangan task ke 3 yg sudah di update");
		task.setSelesai(true);
		Calendar tanggal_dibuat = Calendar.getInstance();
		tanggal_dibuat.set(2011, Calendar.FEBRUARY, 27);

		Calendar tanggal_selesai = Calendar.getInstance();
		tanggal_selesai.set(2012, Calendar.JANUARY, 2);

		db.updateTask(task);
	}

	@Test
	public void testDeleteTask() {
		Task task = new Task();
		task.setId(1L);
		db.deleteTask(task);
	}

}
